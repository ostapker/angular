﻿using Common.DTOs.Project;
using Common.DTOs.Team;
using Common.DTOs.User;
using DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Web_API.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {    
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task CreateProject_ThanResponseWith201AndCorrespondedBody()
        {
            var projectDTO = new ProjectCreateDTO 
            { 
                Name = "new pr name", 
                Description = "new pr description", 
                Deadline = DateTime.Now.AddYears(2), 
                AuthorId = 1, 
                TeamId = 1 
            };

            var jsonInString = JsonConvert.SerializeObject(projectDTO);
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            await _client.DeleteAsync($"api/projects/{createdProject.Id}");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(projectDTO.Name, createdProject.Name);
            Assert.Equal(projectDTO.Description, createdProject.Description);
            Assert.Equal(projectDTO.Deadline, createdProject.Deadline);
            Assert.Equal(projectDTO.AuthorId, createdProject.AuthorId);
            Assert.Equal(projectDTO.TeamId, createdProject.TeamId);
        }

        [Fact]
        public async Task CreateProject_WhenAuthorAndTeamNotExist_ThenResponseCode404()
        {
            var projectDTO = new ProjectCreateDTO
            {
                Name = "new pr name",
                Description = "new pr description",
                Deadline = DateTime.Now.AddYears(2),
                AuthorId = 0,
                TeamId = 0
            };

            var jsonInString = JsonConvert.SerializeObject(projectDTO);
            var responseHttp = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.NotFound, responseHttp.StatusCode);
        }
    }
}

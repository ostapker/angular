﻿using Common.DTOs.Project;
using Common.DTOs.Task;
using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Xunit;
using DAL.Context;

namespace Web_API.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task DeleteTaskById_ThenResponseCode204AndGetByIdCode404()
        {
            var taskDTO = new TaskCreateDTO
            {
                Name = "test task",
                Description = "test description",
                PerformerId = 1,
                ProjectId = 1,
                FinishedAt = DateTime.Now.AddYears(2)
            };

            var jsonInString = JsonConvert.SerializeObject(taskDTO);
            var response = await _client.PostAsync("api/tasks", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var createdTask = await response.Content.ReadAsAsync<TaskDTO>();

            var deleteResponse = await _client.DeleteAsync($"api/tasks/{createdTask.Id}");
            var getResponse = await _client.GetAsync($"api/tasks/{createdTask.Id}");

            Assert.Equal(HttpStatusCode.NoContent, deleteResponse.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTaskById_WhenTaskNotExist_ThenResponseCode404()
        {
            var deleteResponse = await _client.DeleteAsync($"api/tasks/{0}");
            Assert.Equal(HttpStatusCode.NotFound, deleteResponse.StatusCode);
        }

        [Fact]
        public async Task GetUnfinishedTasksByUser_WhenOneUnfinishedTask_ThenListWithSingleTask()
        {
            var userDTO = new UserCreateDTO
            {
                FirstName = "test user",
                LastName = "test",
                Birthday = DateTime.Now.AddYears(-20),
                Email = "test@gmail.com"
            };

            var userJsonInString = JsonConvert.SerializeObject(userDTO);
            var responseUser = await _client.PostAsync("api/users", new StringContent(userJsonInString, Encoding.UTF8, "application/json"));
            var createdUser = await responseUser.Content.ReadAsAsync<UserDTO>();

            var taskDTO = new TaskCreateDTO
            {
                Name = "test task",
                Description = "test description",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                FinishedAt = DateTime.Now.AddYears(2)
            };

            var taskJsonInString = JsonConvert.SerializeObject(taskDTO);
            var responseTask = await _client.PostAsync("api/tasks", new StringContent(taskJsonInString, Encoding.UTF8, "application/json"));
            var createdTask = await responseTask.Content.ReadAsAsync<TaskDTO>();

            var httpResponse = await _client.GetAsync($"api/tasks/unfinishedByUser/{createdUser.Id}");
            var unfinishedTasks = await httpResponse.Content.ReadAsAsync<IEnumerable<TaskDTO>>();

            await _client.DeleteAsync($"api/tasks/{createdTask.Id}");
            await _client.DeleteAsync($"api/users/{createdUser.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Single(unfinishedTasks);
            Assert.Equal(createdTask.Id, unfinishedTasks.FirstOrDefault().Id);
            Assert.Equal(createdTask.State, unfinishedTasks.FirstOrDefault().State);
        }

        [Fact]
        public async Task GetUnfinishedTasksByUser_WhenUserNotExist_ThenResponseCode404()
        {
            var httpResponse = await _client.GetAsync($"api/tasks/unfinishedByUser/{0}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}

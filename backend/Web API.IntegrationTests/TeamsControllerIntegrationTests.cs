﻿using Common.DTOs.Team;
using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Web_API.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task CreateTeam_ThanResponseWith201AndCorrespondedBody()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = "new team name",
            };

            var jsonInString = JsonConvert.SerializeObject(teamDTO);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            await _client.DeleteAsync($"api/teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(teamDTO.Name, createdTeam.Name);
        }

        [Fact]
        public async Task CreateTeam_WhenNameTooLong_ThenResponseCode500()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = new string('f', 101),
            };

            var jsonInString = JsonConvert.SerializeObject(teamDTO);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.InternalServerError, httpResponse.StatusCode);
        }
    }
}
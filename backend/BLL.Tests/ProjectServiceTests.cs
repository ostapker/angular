﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.MappingProfiles;
using BLL.Services;
using Common.DTOs.Task;
using DAL.Entities;
using DAL.UnitOfWork.Implementation;
using DAL.UnitOfWork.Interfaces;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BLL.Tests
{
    public class ProjectServiceTests : IClassFixture<SeedDataFixture>, IDisposable
    {
        private readonly IProjectService _projectService;
        private readonly Mapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly SeedDataFixture _fixture;

        public ProjectServiceTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
            }));
            _fixture = new SeedDataFixture();

            _unitOfWork = new UnitOfWork(_fixture.Context);

            var userService = A.Fake<IUserService>();
            var teamService = A.Fake<ITeamService>();

            _projectService = new ProjectService(_unitOfWork, _mapper, userService, teamService);
        }

        public void Dispose()
        {
            _fixture.Dispose();
            _unitOfWork.Dispose();
            _projectService.Dispose();
        }


        [Fact]
        public async Task DeleteByIdAsync_WhenNotExistingProject_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _projectService.DeleteByIdAsync(0));
        }

        [Fact]
        public async Task GetProjectTasksCountByUser_WhenOneProjectOneUser_ThenTasksCountEqualsAllTasksCount()
        {
            _fixture.TASK_COUNT = 10;
            _fixture.Seed();

            var dictionary = await _projectService.GetProjectTasksCountByUser(1);
            var key = dictionary.Keys.ElementAt(0);

            Assert.Equal(10, dictionary.GetValueOrDefault(key));
        }

        [Fact]
        public async Task GetProjectsInfo_WhenOneProject_ThenProjectShortestTaskEqualsShortestTask()
        {
            _fixture.TASK_COUNT = 10;
            _fixture.Seed();

            var task = await _unitOfWork.Set<TaskEntity>().GetByIdAsync(5);
            task.Name = "";
            await _unitOfWork.Set<TaskEntity>().UpdateAsync(task);
            await _unitOfWork.SaveChangesAsync();


            var projectInfo = (await _projectService.GetProjectsInfo()).FirstOrDefault();

            Assert.Equal(_mapper.Map<TaskDTO>(task), projectInfo.ShortestTask);
        }

        [Fact]
        public async Task GetProjectsInfo_WhenTooShortDescriptionAndTasksCountMoreThen3_ThenTeamMembersCountEqualsZero()
        {
            _fixture.USER_COUNT = 6;
            _fixture.TASK_COUNT = 6;
            _fixture.Seed();

            var project = await _unitOfWork.Set<ProjectEntity>().GetByIdAsync(1);
            project.Description = "short desc";
            await _unitOfWork.Set<ProjectEntity>().UpdateAsync(project);
            await _unitOfWork.SaveChangesAsync();

            var projectInfo = (await _projectService.GetProjectsInfo()).FirstOrDefault();
            

            Assert.Equal(0, projectInfo.TeamMembersCount);
        }

        [Fact]
        public async Task GetProjectsInfo_WhenOneProject_ThenProjectLongestTaskEqualsLongestTask()
        {
            _fixture.TASK_COUNT = 10;
            _fixture.Seed();

            var task = await _unitOfWork.Set<TaskEntity>().GetByIdAsync(5);
            task.Description = new string('f', 1000);
            await _unitOfWork.Set<TaskEntity>().UpdateAsync(task);
            await _unitOfWork.SaveChangesAsync();

            var projectInfo = (await _projectService.GetProjectsInfo()).FirstOrDefault();

            Assert.Equal(_mapper.Map<TaskDTO>(task), projectInfo.LongestTask);
        }
    }
}

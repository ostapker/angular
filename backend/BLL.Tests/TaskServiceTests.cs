﻿using AutoMapper;
using BLL.Interfaces;
using BLL.MappingProfiles;
using BLL.Services;
using Common.DTOs.Task;
using Common.Enums;
using DAL.UnitOfWork.Implementation;
using FakeItEasy;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using BLL.Exceptions;
using Common.DTOs.User;
using DAL.Entities;

namespace BLL.Tests
{
    public class TaskServiceTests: IClassFixture<SeedDataFixture>, IDisposable
    {
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly Mapper _mapper;
        private readonly SeedDataFixture _fixture;

        public TaskServiceTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
            }));
            _fixture = new SeedDataFixture();

            var unitOfWork = new UnitOfWork(_fixture.Context);
            var teamService = A.Fake<ITeamService>();
            _userService = new UserService(unitOfWork, _mapper, teamService);
            var projectService = A.Fake<IProjectService>();

            _taskService = new TaskService(unitOfWork, _mapper, projectService, _userService);
        }

        public void Dispose()
        {
            _fixture.Dispose();
            _userService.Dispose();
            _taskService.Dispose();
        }

        [Fact]
        public async Task GetTasksPerformedByUser_WhenOneTaskMatch_ThenResultContainsOneElement()
        {
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            var taskDTO = A.Fake<TaskCreateDTO>();
            taskDTO.Name = new string('f', 100);
            taskDTO.PerformerId = 1;
            taskDTO.ProjectId = 1;

            await _taskService.CreateAsync(taskDTO);
            taskDTO.Name = "task that match constraints";
            await _taskService.CreateAsync(taskDTO);

            Assert.Single(await _taskService.GetTasksPerformedByUser(1));
        }

        [Fact]
        public async Task UpdateAsync_WhenCreateTaskAndUpdateState_TnenTaskStateIsFinished()
        {
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            var taskDTO = A.Fake<TaskCreateDTO>();
            taskDTO.PerformerId = 1;
            taskDTO.ProjectId = 1;
            var task = await _taskService.CreateAsync(taskDTO);

            var taskUpdateDTO = _mapper.Map<TaskUpdateDTO>(task);
            taskUpdateDTO.State = TaskState.Finished;
            await _taskService.UpdateAsync(taskUpdateDTO);

            Assert.Equal(TaskState.Finished, (await _taskService.GetByIdAsync(task.Id)).State);
        }

        [Fact]
        public async Task UpdateAsync_WhenUnknownTask_TnenThrowUnknownTaskStateException()
        {
            _fixture.Seed();
            var taskDTO = A.Fake<TaskUpdateDTO>();
            taskDTO.Id = 1;
            taskDTO.ProjectId = 1;
            taskDTO.PerformerId = 1; 
            taskDTO.State = (TaskState)100;
            await Assert.ThrowsAsync<UnknownTaskStateException>(() => _taskService.UpdateAsync(taskDTO));
        }

        [Fact]
        public async Task GetFinishedTasksByUser_When2TasksMatchCondition_ThenReturnsCollectionOf2Elements()
        {
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            for (int i = 1; i <= 2; i++)
            {
                var task = A.Fake<TaskEntity>();
                task.PerformerId = 1;
                task.ProjectId = 1;
                task.FinishedAt = DateTime.Now;
                task.State = DAL.Enums.TaskState.Finished;
                _fixture.Context.Tasks.Add(task);
            }
            await _fixture.Context.SaveChangesAsync();

            Assert.Equal(2, (await _taskService.GetFinishedTasksByUser(1)).Count());
        }

        [Fact]
        public async Task GetTasksGroupedByUser_WhenTwoUsersAndAllTasksPerformedBySecond_ThenFirstELementTasksEmptyAndSecondContainsAllTasks()
        {
            _fixture.USER_COUNT = 2;
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            var taskDTO = A.Fake<TaskCreateDTO>();
            taskDTO.PerformerId = 2;
            taskDTO.ProjectId = 1;
            taskDTO.Name = "test";

            for (int i = 1; i <= 10; i++)
                await _taskService.CreateAsync(taskDTO);


            Assert.Empty((await _taskService.GetTasksGroupedByUser()).FirstOrDefault(u => u.User.Id == 1).Tasks);
            Assert.Equal(10, (await _taskService.GetTasksGroupedByUser()).FirstOrDefault(u => u.User.Id == 2).Tasks.Count());
        }

        [Fact]
        public async Task GetTasksGroupedByUser_WhenCreateUsersAndTasks_ThenUsersSortedByFirstNameAndTasksSortedByNameDescending()
        {
            var userDTO = A.Fake<UserCreateDTO>();
            userDTO.FirstName = "BB";
            await _userService.CreateAsync(userDTO);
            userDTO.FirstName = "AA";
            var user =  await _userService.CreateAsync(userDTO);

            var taskDTO = A.Fake<TaskCreateDTO>();
            taskDTO.PerformerId = user.Id;
            taskDTO.Name = "1";
            await _taskService.CreateAsync(taskDTO);
            taskDTO.Name = "12";
            await _taskService.CreateAsync(taskDTO);

            var result = await _taskService.GetTasksGroupedByUser();

            Assert.Equal("AA", result.FirstOrDefault().User.FirstName);
            Assert.Equal("12", result.FirstOrDefault().Tasks.FirstOrDefault().Name);
        }

        [Fact]
        public async Task GetUnfinishedTasksByUser_WhenOneUserAndTwoTasks_ThenListWithTwoTasks()
        {
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            var taskDTO = A.Fake<TaskCreateDTO>();
            taskDTO.PerformerId = 1;
            taskDTO.ProjectId = 1;

            for (int i = 0; i < 2; i++)
                await _taskService.CreateAsync(taskDTO);

            var taskUpdateDTO = A.Fake<TaskUpdateDTO>();
            taskUpdateDTO.Id = 2;
            taskUpdateDTO.PerformerId = 1;
            taskUpdateDTO.ProjectId = 1;
            taskUpdateDTO.State = TaskState.Started;

            await _taskService.UpdateAsync(taskUpdateDTO);

            Assert.Equal(2, (await _taskService.GetUnfinishedTasksByUser(1)).Count());
        }

        [Fact]
        public async Task GetUnfinishedTasksByUser_WhenOneUserAndZeroTasks_ThenEmptyList()
        {
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            Assert.Empty(await _taskService.GetUnfinishedTasksByUser(1));
        }

        [Fact]
        public async Task GetUnfinishedTasksByUser_WhenUserNotExist_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _taskService.GetUnfinishedTasksByUser(0));
        }
    }
}

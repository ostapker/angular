﻿using AutoMapper;
using Common.DTOs.Task;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskDTO, TaskEntity>();
            CreateMap<TaskCreateDTO, TaskUpdateDTO>();
            CreateMap<TaskDTO, TaskUpdateDTO>();

            CreateMap<TaskDTO, TaskFinishedDTO>();

            CreateMap<TaskCreateDTO, TaskEntity>();
        }
    }
}

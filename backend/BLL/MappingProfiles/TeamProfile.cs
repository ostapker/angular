﻿using AutoMapper;
using Common.DTOs.Team;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamEntity, TeamDTO>();
            CreateMap<TeamDTO, TeamEntity>();
            CreateMap<TeamDTO, TeamWithUsersDTO>();

            CreateMap<TeamCreateDTO, TeamEntity>();
        }
    }
}

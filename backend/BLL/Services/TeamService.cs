﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common;
using Common.DTOs.Team;
using Common.DTOs.User;
using DAL.Entities;
using DAL.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepository<TeamEntity> _repository;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<TeamEntity>();
        }

        public async Task<TeamDTO> CreateAsync(TeamCreateDTO teamDTO)
        {
            var team = _mapper.Map<TeamEntity>(teamDTO);
            team.CreatedAt = DateTime.Now;
            await _repository.CreateAsync(team);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await GetByIdAsync(id);
            await _repository.DeleteByIdAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = (await _repository.GetAllAsync())
                                .Include(p => p.Users);
            return teams.Select(team => _mapper.Map<TeamDTO>(team)).AsEnumerable();
        }

        public async Task<TeamDTO> GetByIdAsync(int id)
        {
            var team = (await GetAllAsync())
                            .FirstOrDefault(p => p.Id == id);
            if (team == null)
                throw new NotFoundException("Team", id);
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> UpdateAsync(TeamUpdateDTO teamDTO)
        {
            var team = await _repository.GetByIdAsync(teamDTO.Id);
            if (team == null)
                throw new NotFoundException("Team", teamDTO.Id);

            team.Name = teamDTO.Name;

            await _repository.UpdateAsync(team);
            await _unitOfWork.SaveChangesAsync();

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<IEnumerable<TeamWithUsersDTO>> GetTeamsWithUsers()
        {
            var teams = await GetAllAsync();
            return teams.Where(t => t.Users.All(user => DateTime.Now.Year - user.Birthday.Year > Constants.MinAge) && t.Users.Count > 0)
                        .Select(t =>
                        {
                            t.Users = t.Users.OrderByDescending(user => user.RegisteredAt).ToList();
                            return _mapper.Map<TeamWithUsersDTO>(t);
                        });
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}

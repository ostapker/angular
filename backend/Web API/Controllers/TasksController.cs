﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using Common.DTOs.Task;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            return Ok(await _taskService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] TaskCreateDTO taskDTO)
        {
            var task = await _taskService.CreateAsync(taskDTO);
            return Created($"api/Tasks/{task.Id}", task);
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> Put([FromBody] TaskUpdateDTO taskDTO)
        {
            return Ok(await _taskService.UpdateAsync(taskDTO));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            await _taskService.DeleteByIdAsync(id);
            return NoContent();
        }

        [HttpGet("performedByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksPerformedByUser(int id)
        {
            return Ok(await _taskService.GetTasksPerformedByUser(id));
        }

        [HttpGet("finishedByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskFinishedDTO>>> GetFinishedTasksByUser(int id)
        {
            return Ok(await _taskService.GetFinishedTasksByUser(id));
        }

        [HttpGet("unfinishedByUser/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasksByUser(int id)
        {
            return Ok(await _taskService.GetUnfinishedTasksByUser(id));
        }

        [HttpGet("unfinished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasks()
        {
            return Ok(await _taskService.GetUnfinishedTasks());
        }

        [HttpGet("groupedByUser")]
        public async Task<ActionResult<IEnumerable<UserWithTasksDTO>>> GetTasksGroupedByUser()
        {
            return Ok(await _taskService.GetTasksGroupedByUser());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_API.Enums
{
    public enum ErrorCode
    {
        General = 1,
        NotFound,
        BadArgument
    }
}

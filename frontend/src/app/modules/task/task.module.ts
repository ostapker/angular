import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskInfoComponent } from './components/task-info/task-info.component';
import { TaskLinkComponent } from './components/task-link/task-link.component';
import { TaskNewEditComponent } from './components/task-new-edit/task-new-edit.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutes } from 'src/app/app.routes';



@NgModule({
  declarations: [TaskListComponent, TaskInfoComponent, TaskLinkComponent, TaskNewEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(AppRoutes)
  ]
})
export class TaskModule { }

import { Component, OnDestroy, Input } from '@angular/core';
import { Task } from 'src/app/modules/shared/models/task/task';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-task-link',
  templateUrl: './task-link.component.html',
  styleUrls: ['./task-link.component.css']
})
export class TaskLinkComponent implements OnDestroy {
  @Input() task: Task;

  private unsubscribe$ = new Subject<void>();

  constructor() { }
  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

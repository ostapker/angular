import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from '../../../shared/models/task/task';
import { Subject } from 'rxjs';
import { TaskService } from 'src/app/modules/shared/services/task.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {
  public tasks: Task[] = [];
  public loading = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService
  ) { }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ngOnInit(): void {
    this.getTasks();
  }

  public getTasks(): void {
    this.loading = true;
    this.taskService
      .getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.tasks = resp.body;
        },
        (error) => (this.snackBarService.showErrorMessage(error.message))
      )
      .add(() => (this.loading = false));
  }

  public newTask(): void{
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}

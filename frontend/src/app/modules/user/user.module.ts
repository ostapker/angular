import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { UserLinkComponent } from './components/user-link/user-link.component';
import { UserNewEditComponent } from './components/user-new-edit/user-new-edit.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutes } from '../../app.routes';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [UserListComponent, UserInfoComponent, UserLinkComponent, UserNewEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(AppRoutes)
  ]
})
export class UserModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/modules/shared/models/user/user';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { TaskService } from 'src/app/modules/shared/services/task.service';
import { UserListComponent } from '../user-list/user-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { Task } from 'src/app/modules/shared/models/task/task';
import { Project } from 'src/app/modules/shared/models/project/project';
import { ProjectService } from 'src/app/modules/shared/services/project.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  public user: User;
  public tasks: Task[];
  public projects: Project[];

  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private taskService: TaskService,
    private projectService: ProjectService,
    private userListComponent: UserListComponent,
    private snackBarService: SnackBarService
    ) { }
  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
            const id: string = params[`id`];
            this.getTasks(+id);
            this.getProjects(+id);
            this.getUser(+id);
            this.scroll(id);
        }
      );
  }

  public getTasks(id: number): void {
    this.taskService
      .getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.tasks = resp.body.filter(t => t.performerId === id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  public getProjects(id: number): void {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body.filter(t => t.authorId === id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  public scroll(id: string): void{
    const element = document.getElementById(id);
    const offset = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top - 165;
    window.scrollTo({
      top: offset,
      behavior: 'smooth'
    });
  }

  public getUser(id: number): void {
    this.userService
      .getUser(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.user = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public onEditUser(): void{
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  public onDeleteUser(): void{
    this.userService
      .deleteUser(this.user.id)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.userListComponent.users = this.userListComponent.users.filter(u => u.id !== this.user.id);
            this.snackBarService.showUsualMessage(`User(id: ${this.user.id}) deleted`);
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.router.navigate(['../'], {relativeTo: this.route})));
  }
}

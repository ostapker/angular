import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';

export class CustomValidators {
    static dateMinimum(date: string): ValidatorFn {
      return (control: AbstractControl): ValidationErrors | null => {
        if (control.value == null) {
          return null;
        }

        const controlDate = moment(control.value, 'YYYY-MM-DD');
        if (!controlDate.isValid()) {
          return null;
        }
        const validationDate = moment(date);
        return controlDate.isSameOrAfter(validationDate) ? null : {
          'date-minimum': {
            'date-minimum': validationDate.format('YYYY-MM-DD'),
            actual: controlDate.format('YYYY-MM-DD')
          }
        };
      };
    }

    static dateMaximum(date: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          if (control.value == null) {
            return null;
          }
          const controlDate = moment(control.value, 'YYYY-MM-DD');
          if (!controlDate.isValid()) {
            return null;
          }
          const validationDate = moment(date);
          return controlDate.isSameOrBefore(validationDate) ? null : {
            'date-maximum': {
              'date-maximum': validationDate.format('YYYY-MM-DD'),
              actual: controlDate.format('YYYY-MM-DD')
            }
          };
        };
      }
}

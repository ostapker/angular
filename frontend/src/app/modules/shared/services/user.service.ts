import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { User } from '../models/user/user';
import { NewUser} from '../models/user/new-user';
import { UpdateUser } from '../models/user/update-user';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UserService {
    public routePrefix = '/api/users';

    constructor(private httpService: HttpInternalService) { }

    public getUsers(): Observable<HttpResponse<User[]>> {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
    }

    public getUser(id: number): Observable<HttpResponse<User>> {
        return this.httpService.getFullRequest<User>(`${this.routePrefix}/${id}`);
    }

    public createUser(user: NewUser): Observable<HttpResponse<User>> {
        return this.httpService.postFullRequest<User>(`${this.routePrefix}`, user);
    }

    public updateUser(user: UpdateUser): Observable<HttpResponse<User>> {
        return this.httpService.putFullRequest<User>(`${this.routePrefix}`, user);
    }

    public deleteUser(id: number): Observable<HttpResponse<unknown>> {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }
}

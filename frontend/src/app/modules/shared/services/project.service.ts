import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Project } from '../models/project/project';
import { NewProject } from '../models/project/new-project';
import { UpdateProject } from '../models/project/update-project';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ProjectService {
    public routePrefix = '/api/projects';

    constructor(private httpService: HttpInternalService) { }

    public getProjects(): Observable<HttpResponse<Project[]>> {
        return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
    }

    public getProject(id: number): Observable<HttpResponse<Project>> {
        return this.httpService.getFullRequest<Project>(`${this.routePrefix}/${id}`);
    }

    public createProject(project: NewProject): Observable<HttpResponse<Project>> {
        return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
    }

    public updateProject(project: UpdateProject): Observable<HttpResponse<Project>> {
        return this.httpService.putFullRequest<Project>(`${this.routePrefix}`, project);
    }

    public deleteProject(id: number): Observable<HttpResponse<unknown>> {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }
}

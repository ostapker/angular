import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import { TaskState } from '../models/enums/taskState';

@Directive({
  selector: '[appTaskStatus]'
})
export class TaskStatusDirective {
  @Input('appTaskStatus') taskState: TaskState;

  @HostListener('DOMSubtreeModified') onChange(): void {
    let color = 'black';
    switch (this.taskState){
      case TaskState.Created:
        color = 'yellow';
        break;
      case TaskState.Started:
        color = 'blue';
        break;
      case TaskState.Finished:
        color = 'green';
        break;
      case TaskState.Canceled:
        color = 'red';
        break;
    }
    this.el.nativeElement.style.color = color;
  }

  constructor(private el: ElementRef) {}
}

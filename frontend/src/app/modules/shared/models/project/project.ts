import { User } from '../user/user';
import { Team } from '../team/team';
import { Task } from '../task/task';

export interface Project {
    id: number;
    authorId: number;
    author: User;
    teamId: number;
    team: Team;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    tasks: Task[];
}

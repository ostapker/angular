export interface NewUser {
    teamId: number | null;
    firstName: string;
    lastName: string;
    email: string;
    birthday: Date;
}

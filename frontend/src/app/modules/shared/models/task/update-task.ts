import { TaskState } from '../enums/taskState';

export interface UpdateTask {
    id: number;
    projectId: number;
    performerId: number;
    name: string;
    description: string;
    finishedAt: Date;
    state: TaskState;
}

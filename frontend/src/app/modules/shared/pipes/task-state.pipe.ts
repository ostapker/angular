import { PipeTransform, Pipe } from '@angular/core';
import { TaskState } from '../models/enums/taskState';

@Pipe({
    name: 'stringTaskState',
    pure: false
  })
  export class StringTaskStatePipe implements PipeTransform {

      transform(value: TaskState): string {
        return TaskState[value];
    }
}

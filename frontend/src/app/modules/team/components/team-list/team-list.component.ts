import { Component, OnInit, OnDestroy } from '@angular/core';
import { Team } from 'src/app/modules/shared/models/team/team';
import { Subject } from 'rxjs';
import { TeamService } from 'src/app/modules/shared/services/team.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit, OnDestroy {
  public teams: Team[] = [];
  public loading = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService
  ) { }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ngOnInit(): void {
    this.getTeams();
  }

  public getTeams(): void {
    this.loading = true;
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
        },
        (error) => (this.snackBarService.showErrorMessage(error.message))
      )
      .add(() => (this.loading = false));
  }

  public newTeam(): void{
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamInfoComponent } from './components/team-info/team-info.component';
import { TeamLinkComponent } from './components/team-link/team-link.component';
import { TeamNewEditComponent } from './components/team-new-edit/team-new-edit.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutes } from '../../app.routes';


@NgModule({
  declarations: [TeamListComponent, TeamInfoComponent, TeamLinkComponent, TeamNewEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(AppRoutes)
  ]
})
export class TeamModule { }
